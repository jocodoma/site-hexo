#!/usr/bin/env bash
# Joseph Chen

THEME_DIR="themes/next"

# Check input argument
if [ "x${1}" = x ]; then
    echo -e "\nUsage: ${0} <option>"
    echo -e "<option>: check or update\n"
    exit 1
fi

if [ ${1} != check -a ${1} != update ]; then
    echo -e "[ERROR]: The option is not supported!"
    echo -e "\nUsage: ${0} <option>"
    echo -e "<option>: check or update\n"
    exit 1
fi

# Init submodule
if [ -z "$(ls -A $THEME_DIR)" ]; then
    git submodule update --init --recursive
fi

# List all available releases (tags)
if [ ${1} = check ]; then
    git submodule update --remote --merge ${THEME_DIR}/

    # Display available updates.
    echo -e "Listing all available releases of NexT (themes/next) ..."

    cd ${THEME_DIR}
    git fetch
    #git log --pretty=oneline --abbrev-commit --decorate HEAD..origin/master
    #git log --no-walk --pretty="%h %d %s" --tags --abbrev-commit --decorate=full
    git log --no-walk --pretty=oneline --tags --abbrev-commit --decorate
    cd ../../
fi

# Update NexT
if [ ${1} = update ]; then
    #git submodule update --remote --merge ${THEME_DIR}/
    if [ "x${2}" != x ]; then
        cd ${THEME_DIR}
        git checkout tags/${2}
        cd ../../
    fi
fi
