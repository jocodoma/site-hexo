![Build Status](https://gitlab.com/pages/hexo/badges/master/build.svg)

---

Example [Hexo](https://hexo.io/) website using GitLab Pages.

Learn more about GitLab Pages at https://about.gitlab.com/product/pages/ and the official
documentation https://docs.gitlab.com/ee/user/project/pages/index.html.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
<!-- **Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)* -->
**Table of Contents**

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI](https://about.gitlab.com/product/continuous-integration/), following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: node:8.16.0

variables:
  GIT_SUBMODULE_STRATEGY: recursive

before_script:
  - npm install hexo-cli -g
  - test -e package.json && npm install

pages:
  script:
    - hexo clean && hexo generate
  artifacts:
    paths:
      - public
  cache:
    paths:
      - node_modules
  only:
    - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install](https://hexo.io/docs/index.html#Installation) Hexo and [Hexo Server](https://hexo.io/docs/server.html)
1. Install dependencies: `npm install`
1. Preview your site: `hexo server`
1. Add content
1. Generate your site (optional): `hexo generate`

Read more at Hexo's [documentation](https://hexo.io/docs/).

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [GitLab Pages domain names](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-domain-names).

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

----

@jocodoma
